// @flow

import * as React from 'react';
import {
  Alert,
  AsyncStorage, Dimensions,
  Image, Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  Touchable,
  View
} from "react-native";
import { connect } from 'react-redux';
import HeaderButtons, {
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import RNFS from 'react-native-fs';
import { NavigationEvents } from 'react-navigation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import BottomNav from './BottomNav';
import store from '../store';
import { removeUserData } from '../actions';
import { getConnections } from '../actions/connections';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import EditProfile from "./OnboardingScreens/editProfile/EditProfile";


const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
export const IMAGE_HEIGHT = height /4;
/**
 * Home screen of BrightID
 * ==========================
 */

// header Button
const SimpleLineIconsHeaderButton = (passMeFurther) => (
  // the `passMeFurther` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...passMeFurther}
    IconComponent={SimpleLineIcons}
    iconSize={32}
    color="#fff"
  />
);

// header Button
const MaterialHeaderButton = (passMeFurther) => (
  // the `passMeFurther` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...passMeFurther}
    IconComponent={Material}
    iconSize={32}
    color="#fff"
  />
);

type Props = {
  score: string,
  groupsCount: number,
  name: string,
  connections: Array<{}>,
  navigation: { navigate: () => null },
  photo: string,
};

export class HomeScreen extends React.Component<Props> {


  constructor(){
    super();
    this.state={
      photo:'',name:'',show: false,
    }
  }

  componentWillMount(){
    this.getData();
  }
  getData = async ()=>{
    let value = await AsyncStorage.getItem('userData');
    let object = JSON.parse(value);
    console.log('object;'+object)
    console.log('willmount photoid'+ object.photo.filename)
    this.setState({
      photo:object.photo,
      name:object.name,
    })
  }





  toggleShow() {
    this.setState({
      show: !this.state.show
    })
  }

  handleTextRef = ref => this.view = ref;



  handleDelete=()=>{
    if (__DEV__) {
      Alert.alert(
        'WARNING',
        'Would you like to delete user data and return to the onboarding screen?',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Sure',
            onPress: async () => {
              try {
                this.props.navigation.navigate('Onboarding');
                await AsyncStorage.flushGetRequests();
                await AsyncStorage.clear();
                store.dispatch(removeUserData());
              } catch (err) {
                console.log(err);
              }
            },
          },
        ],
        { cancelable: true },
      );
    }
  }






  render() {
    const {
      navigation,
      name,
      score,
      groupsCount,
      connections,
      photo,
      dispatch,
    } = this.props;


    console.log('filename:'+this.state.photo.filename)
    return (
      <View style={styles.container}>


        <LinearGradient colors={['#f63626', '#f7641e']} style={{zIndex:999999,width:width,flexDirection:'row',justifyContent:'center',alignItems:'center',paddingVertical:9,
          shadowOffset: {width: 0, height: 4},
          shadowColor: 'gray',
          shadowOpacity: 0.29,
          elevation: 6,
        }}>
          <View style={{width:width/3-10,alignItems:'flex-start'}}>

            <SimpleLineIcons name="question" size={33} color="white" />
          </View>

          <View style={{width:width/3,alignItems:'center'}}>

            <Text style={{fontSize:24,color:'white',fontFamily: 'EurostileRegular',fontWeight: '200',alignSelf:'center'}}>
              BrightID
            </Text>

          </View>
          <View style={{width:width/3-10,alignItems:'flex-end'}}>

            <Material name="dots-horizontal" size={35} color="white"
              onPress={() => {
                this.view.transitionTo({top: (this.state.show ? -height/1.5 : 53)});
                this.toggleShow()
              }}

            />

          </View>

        </LinearGradient>



        <TouchableOpacity
          onPress={() => {
            this.toggleShow()
            this.view.transitionTo({top: (this.state.show ? -height/1.5 : 0)});
          }}
          style={{position:'absolute',height:height,width:width,
            backgroundColor:(this.state.show ? 'rgba(0,0,0,0.4)' :null ),
            zIndex:(this.state.show ? 10000 : -1000)
          }}>

        </TouchableOpacity>



        <Animatable.View style={[ {        position: 'absolute',

          opacity: 1,
          backgroundColor:'white',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          top:-height/1.5, zIndex: 10000}]}
                         iterationCount={1}
                         duration={550} direction="alternate" ref={this.handleTextRef}>




          <View
            style={{
            width: width,
            backgroundColor: 'white',
            borderTopRightRadius: 16,
            borderTopLeftRadius: 16,
            zIndex: 9999,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 20,
            shadowOffset: {width: 0, height: -4},
            shadowColor: 'gray',
            shadowOpacity: 0.29,
            elevation: 3,
          }}>

            <TouchableOpacity

              onPress={()=>this.props.navigation.navigate('EditProfile')}

              style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              width: 0.9 * width,
              borderBottomColor: 'gray',
              borderBottomWidth: 0.8
            }}>

              <MaterialIcons style={{marginBottom:9}}  name="edit" size={30} color="black"/>

              <Text style={{
                fontSize: 20,
                marginRight: 20,
                marginLeft:20,
                marginBottom:12,
              }}>
                Edit Profile
              </Text>
            </TouchableOpacity>


            <TouchableOpacity
              onPress={()=>this.handleDelete()}

              style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              width: 0.9 * width,
              marginTop:10
            }}>

              <Entypo style={{marginBottom:9}}  name="cross" size={30} color="black"/>

              <Text style={{
                fontSize: 20,
                marginRight: 20,
                marginLeft:20,
                marginBottom:12,
              }}>
                Delete My BrightID
              </Text>
            </TouchableOpacity>

          </View>
        </Animatable.View>


        <NavigationEvents
          onDidFocus={() => {
            dispatch(getConnections());
          }}
        />

        <View style={styles.mainContainer}>
          <View style={styles.photoContainer}>
            <Image
              source={{
                uri: `file://${RNFS.DocumentDirectoryPath}/photos/${
                  this.state.photo.filename
                }`,
              }}
              style={styles.photo}
              resizeMode="cover"
              onError={(e) => {
                console.log(e.error);
              }}
              accessible={true}
              accessibilityLabel="user photo"
            />
            <Text id="name" style={styles.name}>
              {this.state.name}
            </Text>
          </View>
          <View style={styles.scoreContainer}>
            <Text style={styles.scoreLeft}>Score:</Text>
            <Text id="score" style={styles.scoreRight}>
              {score}
            </Text>
          </View>
          <View style={styles.countsContainer}>
            <View style={styles.countsGroup}>
              <Text id="connectionsCount" style={styles.countsNumberText}>
                {connections.length}
              </Text>
              <Text style={styles.countsDescriptionText}>Connections</Text>
            </View>
            <View style={styles.countsGroup}>
              <Text id="groupsCount" style={styles.countsNumberText}>
                {groupsCount}
              </Text>
              <Text style={styles.countsDescriptionText}>Groups</Text>
            </View>
          </View>

          <View style={styles.connectContainer}>
            <TouchableOpacity
              style={styles.connectButton}
              onPress={() => {
                navigation.navigate('NewConnection');
              }}
              accessible={true}
              accessibilityLabel="Connect"
            >
              <Material name="qrcode-scan" size={26} color="#fff" />
              <Text style={styles.connectText}>New Connection</Text>
            </TouchableOpacity>
          </View>
        </View>

        <BottomNav navigation={navigation} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mainContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  photoContainer: {
    marginTop: 24,
    justifyContent: 'center',
    alignItems: 'center',
    height:height*2/7,
  },
  photo: {
    width: IMAGE_HEIGHT,
    height: IMAGE_HEIGHT,
    borderRadius: IMAGE_HEIGHT/2,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
  },
  name: {
    fontFamily: 'ApexNew-Book',
    fontSize: 30,
    marginTop: 8,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: '#000000',
    textShadowColor: 'rgba(0, 0, 0, 0.32)',
    textShadowOffset: {
      width: 0,
      height: 2,
    },
    textShadowRadius: 4,
    shadowColor: 'rgba(0,0,0,0.32)',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
  },
  connectContainer: {
    width: '100%',
    height:height/7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    flex: 1,
    // marginTop: 17,
    flexDirection: 'row',
  },
  scoreContainer: {
    borderBottomColor: '#e3e1e1',
    borderBottomWidth: 1,
    width: '80%',
    height:height/7,
    // marginTop: 12,
    // paddingBottom: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scoreLeft: {
    fontFamily: 'ApexNew-Book',
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: '#9b9b9b',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 5,
    paddingTop: 3.5,
  },
  scoreRight: {
    fontFamily: 'ApexNew-Medium',
    fontSize: 22,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: '#139c60',
    alignItems: 'center',
    justifyContent: 'center',
  },
  countsContainer: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    height:height/7,
    width: '80%',
    marginTop: 12,
    borderBottomColor: '#e3e1e1',
    borderBottomWidth: 1,
    paddingBottom: 12,
  },
  countsDescriptionText: {
    fontFamily: 'ApexNew-Book',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
  },
  countsNumberText: {
    fontFamily: 'ApexNew-Book',
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
  },
  countsGroup: {
    flex: 1,
  },
  connectButton: {
    height: height/10,
    // paddingTop: 13,
    // paddingBottom: 12,
    width: '80%',
    borderRadius: 6,
    backgroundColor: '#4a90e2',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: { width: 20, height: 20 },
    shadowRadius: 20,
    elevation: 1,
  },
  connectText: {
    fontFamily: 'ApexNew-Medium',
    fontSize: 22,
    color: '#fff',
    marginLeft: 18,
  },
});

export default connect((state) => state.main)(HomeScreen);
