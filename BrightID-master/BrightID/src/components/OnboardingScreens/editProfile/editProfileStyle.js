import { Dimensions, StyleSheet } from "react-native";

const window = Dimensions.get('window');
export const HEIGHT = window.height;
export const IMAGE_HEIGHT = window.height / 3;
export const IMAGE_HEIGHT_SMALL = window.width /7;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addPhotoContainer: {
    height:HEIGHT*2/7,
    resizeMode: 'contain',
    paddingTop:20,
  },
  textInputContainer: {
    height:HEIGHT*2/7,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop:50,
  },
  buttonContainer: {
    height:HEIGHT*2/7,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  addPhoto: {
    borderWidth: 1,
    borderColor: '#979797',
    height: IMAGE_HEIGHT,
    width: IMAGE_HEIGHT,
    borderRadius: IMAGE_HEIGHT/2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoContainer: {
    marginTop: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photof: {
    width: 142,
    height: 142,
    borderRadius: 71,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
  },
  photo: {
    width: IMAGE_HEIGHT,
    height: IMAGE_HEIGHT,
    borderRadius: IMAGE_HEIGHT/2,
    borderWidth:0.5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    justifyContent:'center',
    alignItems: 'center'
  },
  hidden: {
    display: 'none',
  },
  addPhotoText: {
    fontFamily: 'ApexNew-Book',
    color: 'white',
    marginBottom: 11,
    marginTop: 11,
    fontSize: 18,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    fontWeight: 'bold',
  },
  midText: {
    fontFamily: 'ApexNew-Book',
    fontSize: 18,
  },
  textInput: {
    fontFamily: 'ApexNew-Light',
    fontSize: 36,
    fontWeight: '300',
    fontStyle: 'normal',
    letterSpacing: 0,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#9e9e9e',
    height: 60,
    marginHorizontal: 5,
    marginVertical: 5,
    width: window.width - 100,
    textAlign: 'center',
    padding:10,

  },
  buttonInfoText: {
    fontFamily: 'ApexNew-Book',
    color: '#9e9e9e',
    fontSize: 14,
    width: 298,
    textAlign: 'center',
  },
  editBrightIdButton: {
    backgroundColor: '#428BE5',
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom:10,
  },
  buttonInnerText: {
    fontFamily: 'ApexNew-Medium',
    color: '#fff',
    fontWeight: '600',
    fontSize: 18,
  },
  loader: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom:10,
  },
});



export default styles
