// @flow

import * as React from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  View,AsyncStorage,ImageBackground
} from 'react-native';
import styles from './editProfileStyle'
import {  NavigationActions } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-spinkit';
import { connect } from 'react-redux';
import HeaderButtons, {
  HeaderButton,
  Item,
} from 'react-navigation-header-buttons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import {  fakeUserAvatar } from '../actions';
import { mimeFromUri } from '../../../utils/images';
import RNFS from 'react-native-fs';
import { createConnectionPhotoDirectory, savePhoto } from "../../../utils/filesystem";
import { setUserData } from "../../../actions/index";
import nacl from "tweetnacl/nacl";
import { b64ToUrlSafeB64, uInt8ArrayToB64 } from "../../../utils/encoding";


type Props = {
  name: string,
  photo: string,
};
type State = {
  name: string,
  imagePicking: boolean,
  photo: { uri: string },
  creatingBrightId: boolean,
};

// header Button
const IoniconsHeaderButton = (passMeFurther) => (
  // the `passMeFurther` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...passMeFurther}
    IconComponent={Ionicons}
    iconSize={32}
    color="#fff"
  />
);

class EditProfile extends React.Component<Props, State> {

constructor(){
  super();
}
componentWillMount(){
  this.getName();
}

getName= async () => {
  let value = await AsyncStorage.getItem('userData');
  let object = JSON.parse(value);
  this.setState({
    photoUri:object.photo.uri,
    name:object.name,
    photo:object.photo,
    imagePicking: false,
    newPhoto:null,
  })

}


  static navigationOptions = {
    title: 'Edit Profile',
    headerBackTitle: 'EditProfile',
    headerStyle: {
      backgroundColor: '#f48b1e',
    },
    headerTitleStyle: {
        textAlign: 'center',
        flexGrow:1,
        alignSelf:'center',
    },
    headerRight: (
      <View></View>
    ),
  };

  state = {
    name: '',
    photo: { uri: '' },
    creatingBrightId: false,
  };

  imagePickingFalse = () => {

    this.setState({
      imagePicking: false
    })
  };

  imagePickingTrue = () => {
        this.setState({
          imagePicking: true
        })
  };

  randomAvatar = async (): Promise<void> => {
    try {
      const randomImage: string = await fakeUserAvatar();
      const photo = {
        uri: `data:image/jpeg;base64,${randomImage}`,
      };
      this.setState({
        photo,
      });
      this.imagePickingFalse();
    } catch (err) {
      console.log(err);
    }
  };

  getPhoto = () => {
    // for full documentation on the Image Picker api
    // see https://github.com/react-community/react-native-image-picker
    const options = {
      title: 'Select Photo',
      mediaType: 'photo',
      maxWidth: 180,
      maxHeight: 180,
      quality: 0.8,
      allowsEditing: true,
      loadingLabelText: 'loading photo...',
      customButtons: [],
    };

    if (__DEV__) {
      options.customButtons = [{ name: 'random', title: 'Random Avatar' }];
    }
    // loading UI to account for the delay after picking an image
    this.imagePickingTrue();

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        this.imagePickingFalse();
      } else if (response.error) {
        Alert.alert('ERROR', response.error);
        this.imagePickingFalse();
      } else if (response.customButton) {
        this.randomAvatar();
      } else {
        const mime = mimeFromUri(response.uri);
        const photo = {
          uri: `data:${mime};base64,${response.data}`,
        };
        this.state.photoUri=photo.uri;
        this.setState({
          photo,
          imagePicking: false,
          newPhoto:photo.uri
        });
      }
    });
  };

  editBrightID = async () => {
    try {
      const { photo, name } = this.state;
      const { navigation, dispatch } = this.props;
      this.setState({
        creatingBrightId: true,
      });

      if (!name) {
        consol.log("in name")
        this.setState({
          creatingBrightId: false,
        });
        return Alert.alert('BrightID Form Incomplete', 'Please add your name');
      }
      if (!photo) {
        this.setState({
          creatingBrightId: false,
        });
        return Alert.alert('BrightID Form Incomplete', 'A photo is required');
      }
      await AsyncStorage.setItem('name', name);

      this.handleEdit();
    } catch (err) {
      this.setState({
        creatingBrightId: false,
      });
    }
  };

  handleEdit = async () => {
    const { navigation ,dispatch} = this.props;
    const { publicKey, secretKey } = nacl.sign.keyPair();
    const b64PubKey = uInt8ArrayToB64(publicKey);
    const safePubKey = b64ToUrlSafeB64(b64PubKey);
    await createConnectionPhotoDirectory();
    const filename = await savePhoto({ safePubKey, base64Image: this.state.photo.uri });
    let data = await AsyncStorage.getItem("userData");
    let userData = JSON.parse(data);
    userData.safePubKey=safePubKey;
    userData.photo.filename=filename;
    userData.name = this.state.name;
    let result = await AsyncStorage.setItem('userData', JSON.stringify(userData));
    data = await AsyncStorage.getItem("userData");
    userData = JSON.parse(data);
    await dispatch(setUserData(userData));
    navigation.reset([NavigationActions.navigate({ routeName: 'Home' })], 0)
  };



  renderButtonOrSpinner = () =>
    !this.state.creatingBrightId ? (
      <TouchableOpacity
        style={styles.editBrightIdButton}
        onPress={this.editBrightID}
      >
        <Text style={styles.buttonInnerText}>Update My BrightID</Text>
      </TouchableOpacity>
    ) : (
      <View style={styles.loader}>
        <Text>Update Bright ID...</Text>
        <Spinner isVisible={true} size={47} type="Wave" color="#4990e2" />
      </View>
    );

  render() {
    const { imagePicking , photo} = this.state;
    photo.uri=this.state.photoUri;


    const AddPhotoButton = this.state.newPhoto === null ? (
      <TouchableOpacity
        onPress={this.getPhoto}
        accessible={true}
        accessibilityLabel="edit photo"
      >
      <ImageBackground style={styles.photo} imageStyle={styles.photo}
                       source={{
        uri: `file://${RNFS.DocumentDirectoryPath}/photos/${
          this.state.photo.filename
          }`,
      }} onPress={this.getPhoto} >
        <Text style={styles.addPhotoText}>Update Photo</Text>
        <SimpleLineIcons size={48} name="camera" color="white" />
              </ImageBackground>
      </TouchableOpacity>
    ) : (
      <TouchableOpacity
        onPress={this.getPhoto}
        style={styles.addPhoto}
        accessible={true}
        accessibilityLabel="add photo"
      >
      <ImageBackground style={styles.photo} imageStyle={styles.photo} source={{uri:this.state.newPhoto}} onPress={this.getPhoto} >
        <Text style={styles.addPhotoText}>Update Photo</Text>
        <SimpleLineIcons size={48} name="camera" color="white" />
              </ImageBackground>
      </TouchableOpacity>
    );

    return (

      <KeyboardAvoidingView style={styles.container} behavior="padding">

        <StatusBar
          barStyle="default"
          backgroundColor={Platform.OS === 'ios' ? 'transparent' : '#000'}
          translucent={false}
        />
          <View style={styles.addPhotoContainer}>
          {!imagePicking ? (
            AddPhotoButton
          ) : (
            <Spinner isVisible={true} size={79} type="Bounce" color="#4990e2" />
          )}
        </View>
        <View style={styles.textInputContainer}>
          <Text style={styles.midText}>What do your friends know you by?</Text>
          <TextInput
            onChangeText={(name) => this.setState({ name })}
            value={this.state.name}
            placeholder="Name"
            placeholderTextColor="#9e9e9e"
            style={styles.textInput}
            autoCapitalize="words"
            autoCorrect={false}
            textContentType="name"
            underlineColorAndroid="transparent"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonInfoText}>


            Your name and photo will never be shared with apps or stored on
            servers
          </Text>
          {this.renderButtonOrSpinner()}
        </View>
      </KeyboardAvoidingView>
    );
  }
}


export default connect(null)(EditProfile);
